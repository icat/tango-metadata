'''
Created on Jun 6, 2014

@author: cleva
'''

import logging

class TangoLoggingHandler(logging.Handler):
    '''
    classdocs
    '''

    def __init__(self, pyta):
        '''
        Constructor
        '''
        logging.Handler.__init__(self)
        self.pyta = pyta
    
    def emit(self, record):
        if record.levelno == logging.DEBUG:
            self.pyta.debug_stream(record.getMessage())
        elif record.levelno == logging.INFO:
            self.pyta.info_stream(record.getMessage())
        elif record.levelno == logging.WARNING:
            self.pyta.warn_stream(record.getMessage())
        elif record.levelno == logging.ERROR:
            self.pyta.error_stream(record.getMessage())
        elif record.levelno == logging.CRITICAL:
            self.pyta.fatal_stream(record.getMessage())