'''
Created on Nov 17, 2014

@author: cleva
'''

import h5py
from datetime import datetime
import re
import os
import numpy
import xml.etree.ElementTree as ET
import logging
import traceback

class HDF5FileWriter():
    '''
    Class for writing an NeXus compliant HDF5 file based on XML configuration
    '''
    PHASE_INITIAL = 'initial'
    PHASE_FINAL = 'final'
    _LINK_PHASE_ = 'link'

    _DEFINITION_FILE_LOCATION = os.path.join(os.path.dirname(__file__), "hdf5_cfg.xml")
    _PARAM_REGEX = re.compile('\A\${(.*)}\Z')
    _GROUP_NAME_ATTRIBUTE = 'groupName'
    _RECORD_TYPE_ATTRIBUTE = 'record'
    _ESRF_ATTRIBUTE_HEADER = "ESRF_"
    _LINK_TARGET_ATTRIBUTE = 'ref'

    ENTRY_KEY = 'entry'
    _ENTRY_FORMAT = 'entry_{0:0>4d}: {1} - {2}'

    def __init__(self, metadataManagerName):
        '''
        Constructor
        '''
        self.root = ET.parse(HDF5FileWriter._DEFINITION_FILE_LOCATION).getroot()
        self.snapshotName = None
        self.metadataManagerName = metadataManagerName
        self.logger = logging.getLogger("HDF5FileWriter({})".format(metadataManagerName))


    '''
    It produces a new entry containing the current values for all the final parameters defined on hdf5_cfg.xml file
    '''
    def appendSnapshotTo(self, fileName, params, phase, outputFileList, snapshotName):
        self.appendTo(fileName, params, phase, snapshotName)

    def appendTo(self, fileName, params, phase, outputFileList, snapshotName=None):
        '''
        Append params metadata into fileName for a given phase. Returns the value for ENTRY_KEY.
        If phase is PHASE_FINAL, the params dictionary must contain the key ENTRY_KEY with the value returned during PHASE_INITIAL.
        If the key is missing this will raise and exception.
        If the key is wrong data will be stored in the wrong NXentry, possibly throwing an exception if trying to write an existing group/field. 
        The method returns the value for ENTRY_KEY, which is the one given in params if PHASE_FINAL or a created one in PHASE_INITIAL.
        '''

        f = None
        self.snapshotName = snapshotName
        try:
            if os.path.exists(fileName):
                f = h5py.File(fileName, 'r+')
            else:
                f = h5py.File(fileName, 'w')
            # we need to have an ENTRY reference
            # for phase INITIAL it is created
            # for phase FINAL it should be in the dict
            if phase == HDF5FileWriter.PHASE_INITIAL:
                entry = HDF5FileWriter._ENTRY_FORMAT.format(len(f.keys()), params.get('sampleName', 'NO_SAMPLE'), params.get('datasetName', 'NO_SCAN'))
                #if HDF5FileWriter.ENTRY_KEY in params:
                #    self.logger.warn("Replacing existing ENTRY %s by new one %s" % (params.get(HDF5FileWriter.ENTRY_KEY), entry))
                params[HDF5FileWriter.ENTRY_KEY] = entry
            elif phase == HDF5FileWriter.PHASE_FINAL:
                if HDF5FileWriter.ENTRY_KEY not in params:
                    raise ValueError("Dictionary does not contain a %s key while in FINAL phase" % HDF5FileWriter.ENTRY_KEY)
                entry = params.get(HDF5FileWriter.ENTRY_KEY)
            else:
                raise NameError("Phase should be %s or %s" % (HDF5FileWriter.PHASE_INITIAL, HDF5FileWriter.PHASE_FINAL))
            self._writeNode(f, self.root, "", params, phase)
            self._writeNode(f, self.root, "", params, HDF5FileWriter._LINK_PHASE_)
            self._writeFiles(f, entry, outputFileList)
            #self.logger.info("Dataset %s exported to %s, %s phase" % (params.get('datasetName', ''), fileName, phase))
            return entry
        except Exception as e:
             #traceback.print_exc()
             self.logger.error("Error %s. metadatamanager=%s" % (e, self.metadataManagerName))
        finally:
            if f:
                try:
                    f.close()
                except Exception as e:
                    self.logger.warn("Error closing file %s: %s. metadatamanager=%s" % (fileName, e, self.metadataManagerName))


    def _writeGroupNode(self, hdffile, currentNode, parentPath, params, phase):
        name = currentNode.get(HDF5FileWriter._GROUP_NAME_ATTRIBUTE)
        value = self._getValueFor(name, params)
        currentPath = parentPath + "/" + value
        #self.logger.debug("Processing group %s (%s)" % (name, value))

        # Recursion on group children
        for child in currentNode:
            self._writeNode(hdffile, child, currentPath, params, phase)

        # set attributes from xml attribs
        g = hdffile.get(currentPath, None)
        if g is not None:
            if len(g.attrs) == 0:
                #self.logger.debug("Recording group %s (%s) attributes. metadatamanager=%s" % (name, value, self.metadataManagerName))
                d = currentNode.attrib.copy()
                d.pop(HDF5FileWriter._GROUP_NAME_ATTRIBUTE)
                for k, v in d.items():
                    if not k.startswith(self._ESRF_ATTRIBUTE_HEADER):
                        g.attrs[k] = v
            #else:
            #    self.logger.debug("Group %s (%s) already created, skipping" % (name, value))
        #else:
        #    self.logger.debug("Skipping empty group %s (%s)" % (name, value))

    def _writeLinkNode(self, hdffile, currentNode, parentPath, params, phase):
        if phase != HDF5FileWriter._LINK_PHASE_:
            return;
        name = currentNode.get(HDF5FileWriter._GROUP_NAME_ATTRIBUTE)
        value = self._getValueFor(name, params)
        currentPath = parentPath + "/" + value
        ref = "/" + params[HDF5FileWriter.ENTRY_KEY] + currentNode.get(HDF5FileWriter._LINK_TARGET_ATTRIBUTE)
        # register only link to existing fields/groups
        if hdffile.get(ref, None) is None:
            return;
        if hdffile.get(currentPath, None) is not None:
            return;
        # create group, creates the whole hierarchy
        # assign link

        hdffile[currentPath] = h5py.SoftLink(ref)
        # set attributes from xml attribs
        d = currentNode.attrib.copy()
        d.pop(HDF5FileWriter._GROUP_NAME_ATTRIBUTE)
        for k, v in d.items():
            if not k.startswith(self._ESRF_ATTRIBUTE_HEADER):
                hdffile[currentPath].attrs[k] = v

    def _writeFiles(self, f, entry, outputFileList):
        if len(outputFileList) > 0:
            measurement = f[entry].create_group('measurement')
            measurement.attrs["NX_class"] = 'NXcollection'
            measurement.create_dataset("files", data=outputFileList)


    def _writeParameterNode(self, hdffile, currentNode, parentPath, params, phase):
        nodePhase = currentNode.get(HDF5FileWriter._RECORD_TYPE_ATTRIBUTE, HDF5FileWriter.PHASE_INITIAL)

        # self.logger.info("\t\t WRITE NODE: [{0}] [{1}] [{2}] [{3}] [{4}]]".format(str(currentNode.text), self._getValueFor(currentNode.text, params), str(parentPath), str(phase), str(nodePhase)))
        # Skip everything if not in phase
        if phase != nodePhase:
            return;
        name = currentNode.tag
        currentPath = parentPath + '/' + name

	    # Replace group if it is a snapshot
        if "final" in currentPath:
	        if self.snapshotName:
	            currentPath = currentPath.replace("final", self.snapshotName)

	    # For a attribute with record="final" within not a final group
        if self.snapshotName:
	        if self.snapshotName not in currentPath:
	            return

        dt = currentNode.get('NAPItype')
        value = self._getValueFor(currentNode.text, params)
        # early return for missing param
        if value is None:
            #self.logger.debug("Skipping field %s, no value for %s" % (name, currentNode.text))
            return;
        # create field with correct type and value
        #self.logger.debug("Recording field %s = %s (%s)" % (name, currentNode.text, value))
        if dt is None or dt == 'NX_CHAR':
            hdffile[currentPath] = numpy.string_(value)
        elif dt == 'NX_DATE_TIME':
            try:
                hdffile[currentPath] = numpy.string_(datetime.isoformat(value))
            except Exception as e:
                self.logger.error("Unable to format date value %s: %s" % (value, e))
                hdffile[currentPath] = numpy.string_("ERROR")
        elif dt == 'NX_INT':
            if "ERROR" != value and "None" != value:
                try:
                    hdffile[currentPath] = int(value)
                except Exception as e:
                    self.logger.error("Unable to format int value %s: %s" % (value, e))
                    hdffile[currentPath] = int("-9999")
            else:
                hdffile[currentPath] = int("-9999")
        else:
            if "ERROR" != value and "None" != value:
                try:
                    hdffile[currentPath] = float(value)
                except Exception as e:
                    self.logger.error("Unable to format double value %s: %s. metadatamanager=%s" % (value, e, self.metadataManagerName))
                    hdffile[currentPath] = float("-9999")
            else:
                hdffile[currentPath] = float("-9999")

        # set attributes from xml attribs
        d = currentNode.attrib.copy()
        # skip the 'record' attribute (phase)
        if HDF5FileWriter._RECORD_TYPE_ATTRIBUTE in d:
            d.pop(HDF5FileWriter._RECORD_TYPE_ATTRIBUTE)
        # loop on others, skip ESRF specific ones (not needed anymore)
        for k, v in d.items():
            if not k.startswith(self._ESRF_ATTRIBUTE_HEADER):
                hdffile[currentPath].attrs[k] = v

    def _writeNode(self, hdffile, currentNode, parentPath, params, phase):

        # Depth first object creation, parent groups are created by children as needed
        # Attributes are set after recursion if the group was created
        # Done on all phases to accommodate for all cases
        if currentNode.tag == "group":
            self._writeGroupNode(hdffile, currentNode, parentPath, params, phase)

        # Record links in final pass
        # This way we are sure link target are already there
        elif currentNode.tag == "link":
           self._writeLinkNode(hdffile, currentNode, parentPath, params, phase)

        # Record parameters when we need to
        # Default pass is initial
        else:
           self. _writeParameterNode(hdffile, currentNode, parentPath, params, phase)

    def _getValueFor(self, string, params):
        if string is None:
            return None
        m = HDF5FileWriter._PARAM_REGEX.match(string)
        if m:
            return params.get(m.group(1), None)
        else:
            return string
