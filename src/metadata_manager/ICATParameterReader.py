import sys
import http.client, urllib.request, urllib.parse, urllib.error
import json
import sys, traceback
import logging

from metadata_manager.ParameterDefinition import ParameterDefinition

class ICATParameterReader:
    def __init__(self, metadatamanager, authenticationPlugin, username, password, server, port):
        self.metadatamanager = metadatamanager
        self.data = []
        self.authenticationPlugin = authenticationPlugin
        self.username = username
        self.password = password
        self.server = server
        self.port = port
        self.logger = logging.getLogger("ICATParameterReader({})".format(metadatamanager))

    def __getSessionId(self, authPlugin, username, password, server, port ):
        params = urllib.parse.urlencode({'json': '{"plugin":"' + authPlugin +'","credentials":[{"username":"'+ username + '"},{"password":"' + password + '"}]}'})
        headers = {"Content-type": "application/x-www-form-urlencoded"}       
        conn = http.client.HTTPSConnection(server,port)
        conn.request("POST", "/icat/session", params, headers)
        response = conn.getresponse()
        if response.status == 200:
            sessionId = json.loads(response.read())['sessionId']
            return sessionId                            
        return None

    def __getParameterTypeList(self, sessionId, server, port):      
        conn = http.client.HTTPSConnection(server, port)
        query = "/icat/entityManager?sessionId=" + sessionId + "&query=select p from ParameterType p"   
        conn.request("GET", query.replace(' ', '%20'), {},  {})
        response = conn.getresponse()
        list = []
        if response.status == 200:
            parameterTypes = json.loads(response.read())
            for param in parameterTypes:
                list.append(str(param['ParameterType']['name']))           
            return list
        return None

    '''
        This method compares a list of parameters with the current defined parameters in the ICAT DB
    '''
    def compare(self, parameters, labels):              
        sessionId = self.__getSessionId(self.authenticationPlugin , self.username, self.password, self.server, self.port)

        parameterTypes = []        
        if sessionId is not None:
            parameterTypes = self.__getParameterTypeList(sessionId, self.server, self.port)
                        
        missing = []
        # Finding missing parameters       
        for parameter in list(parameters) + list(labels):
            try:
                parameterName = ParameterDefinition(parameter).getParameterName()            
                if parameterName not in parameterTypes:
                    print("**********--->" + parameterName + " is not in ICAT")
                    missing.append(parameterName)
            except:
                print("--->" + parameterName + " got a problem when retrieving the name")
                traceback.print_exc(file=sys.stdout)
        return self.__getSummary(missing)

    def __getSummary(self, missingParameters):
        text = []        
        if len(missingParameters) > 0:
            text.append('----------------------------------------------------')
            if len(missingParameters) > 1:
                text.append('\n\n[ERROR] %s parameters are unknown by ICAT. Ingestion will fail.'%(str(len(missingParameters))))
            else:
                text.append('\n\n[ERROR] %s parameter is unknown by ICAT. Ingestion will fail.'%(str(len(missingParameters))))
            self.logger.info("Parameter list is not OK compared to ICAT database. missing=%s metadatamanager=%s"%(str(missingParameters), self.metadatamanager))
        else:
            self.logger.info("Parameter list is OK compared to ICAT database. metadatamanager=%s", (self.metadatamanager))
            text.append("Parameter list is OK")
        for parameter in missingParameters:
            text.append('[Parameter] %s'%(parameter))
        return '\n'.join(text)


 
