# ##########################################################################
#
# Copyright (c) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


import os
import sys
import string
import logging
import traceback

from metadata_manager.HDF5FileWriter import HDF5FileWriter
from metadata_manager.FileExplorer import FileExplorer
from metadata_manager.icat_ingest_metadata.dataset import dataset, parameter, sample, datafile

class MetadataWorker():
    '''
    Class containing the login of the metadata engine
    '''
    # Allowed chars for folder and file names
    validFilenameChars = ".-_%s%s" % (string.ascii_letters, string.digits)


    def __init__(self, metadataManagerName):
        '''
        Constructor
        '''
        self.logger = logging.getLogger("MetadataWorker({})".format(metadataManagerName))
        self.entries = []
        self.writer = HDF5FileWriter(metadataManagerName)
        self.fileExplorer   = FileExplorer()
        self.metadataManagerName = metadataManagerName

        '''
        Parameters
        '''
        self.__technique = None

    def setTechnique(self, technique):
        if (technique):
            self.__technique = technique.lower()

    def getTechnique(self):
        return self.__technique

    def createDataset(self, complete, attr_proposal_read,beamlineID, attr_datasetName_read, attr_dataFolder_read, startDate,endDate, labels_map, datasetParamMap, attr_sampleName_read, sampleParamMap, attr_datafileList_read):
        self.attr_dataFolder_read = attr_dataFolder_read
        # dataset basic parameters
        dts = dataset(complete, attr_proposal_read,
                      beamlineID,
                      attr_datasetName_read,
                      attr_dataFolder_read,
                      startDate, endDate)
        # add the dataset parameters
        for n, v in datasetParamMap.items():
            # register only non empty parameters
            if v:
                dts.add_parameter(parameter(n, v))
        # add the dataset labels
        for n, v in labels_map.items():
            # register only non empty parameters
            if v:
                dts.add_parameter(parameter(n, v))
        # sample and sample parameters, if any

        if attr_sampleName_read:
            smpl = sample(attr_sampleName_read)
            for n, v in sampleParamMap.items():
                # register only non empty parameters
                # self.info_stream("sample parameter Name: " + str(parameter(n, v).getName()))
                # self.info_stream("sample parameter Value: " + str(parameter(n, v).getValue()))
                if v:
                    smpl.add_parameter(parameter(n, v))
            dts.set_sample(smpl)
        # datafiles, if any
        # datafile path is created from data folder and file name
        for f in attr_datafileList_read:
            dts.add_datafile(datafile(os.path.join(attr_dataFolder_read, f)))
        # return the created dataset object
        return dts


    def toDictionary(self, dataset, sampleParamMap, initial=None):
        d = dict()
        # add parameters and labels
        for p in dataset.get_parameter():
            d[p.get_name()] = p.get_value()
        # add parameters from sample
        for key in sampleParamMap:
            d[key] = sampleParamMap[key]
        # add main proposal/sample/dataset parameters
        d['proposal']   = dataset.get_investigation()
        d['beamlineID'] = dataset.get_instrument()
        d['datasetName']   = dataset.get_name()
        d['location']   = dataset.get_location()
        d['startDate']  = dataset.get_startDate()
        d['endDate']    = dataset.get_endDate()
        d['sampleName'] = dataset.get_sample().get_name()
        return d

    def addParametersToDataSet(self, dataset, parameters):
        for parameter in parameters:
             dataset.add_parameter(parameter)
        return dataset

    def exportInitial(self, dataset, metadatafilename, entries, files, parameters, sampleParamMap):
        try:
            self.entries = entries
            dataset = self.addParametersToDataSet(dataset, parameters)
            # transform
            params = self.toDictionary(dataset, sampleParamMap, initial=True)

            # export single dataset file
            self.logger.debug(self.entries)
            self.entries[metadatafilename] = self.writer.appendTo(metadatafilename, params, HDF5FileWriter.PHASE_INITIAL, [])
            self.logger.debug(self.entries)


            # export to all global files
            for f in files:
                if HDF5FileWriter.ENTRY_KEY in params:
                    del params[HDF5FileWriter.ENTRY_KEY]
                try:
                    self.logger.warn("Exporting parameters to master. file=%s metadatamanager=%s" %(f, self.metadataManagerName))
                    entries[f] = self.writer.appendTo(f, params, HDF5FileWriter.PHASE_INITIAL, [])
                except Exception as e:
                    self.logger.warn("Error exporting %s: %s. metadatamanager=%s" %(f, e, self.metadataManagerName))
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
        return self.entries





    def exportFinal(self, dataset, metadatafilename, entries, files, attr_metadataFile_read, attr_proposal_read, attr_sampleName_read, attr_datasetName_read, parameters, client):
        try:
            # No more export to XML file
            #xmlfilename = self.MakeFileName('data', 'xml',  attr_metadataFile_read, attr_proposal_read, attr_sampleName_read, attr_datasetName_read,False)

            dataset = self.addParametersToDataSet(dataset, parameters)
            # send to ingestion
            client.sendObject(dataset)

            # keep local xml for reference
            #self.ExportToXMLFile(dataset, xmlfilename)
            # transform
            sampleParamMap = []
            params = self.toDictionary(dataset,sampleParamMap, initial=True)

            # export single dataset file
            if entries[metadatafilename] is not None:
                self.logger.info("Exporting HDF5. metadatafilename=%s metadatamanager=%s" %(str(entries[metadatafilename]), self.metadataManagerName))
            else:
                self.logger.info("Exporting HDF5 metadatafilename is None. metadatamanager=%s" %(self.metadataManagerName))
            params[HDF5FileWriter.ENTRY_KEY] = entries[metadatafilename]

            try:
                if self.attr_dataFolder_read:
                        outputFiles = self.getFiles( self.attr_dataFolder_read)
                self.writer.appendTo(metadatafilename, params, HDF5FileWriter.PHASE_FINAL, outputFiles)

            except Exception as e:
                self.logger.warn("Error exporting. metadatafilename=%s  error=%s metadatamanager=%s" %(metadatafilename, e, self.metadataManagerName))
            # export to all global files
            for f in files:
                if f in entries:
                    params[HDF5FileWriter.ENTRY_KEY] = entries[f]
                    try:
                        entries[f] = self.writer.appendTo(f, params, HDF5FileWriter.PHASE_FINAL, outputFiles)
                    except Exception as e:
                        self.logger.warn("Error exporting. file=%s error=%s metadatamanager=%s" %(f, e, self.metadataManagerName))
                else:
                    self.logger.warn("Not trying to export to %s, entry not registered. metadatamanager=%s" % (f, self.metadataManagerName))

        except Exception as e:
            traceback.print_exc(file=sys.stdout)


    def DefaultFileName(self, attr_proposal_read, attr_sampleName_read, attr_datasetName_read):
        return self.Sanitize(attr_proposal_read + "-" + attr_sampleName_read + "-" + attr_datasetName_read)

    def Sanitize(self, filename):
        return ''.join(c if c in self.validFilenameChars else '_' for c in filename)

    def MakeFileName(self, folder, extension, attr_metadataFile_read, attr_proposal_read, attr_sampleName_read, attr_datasetName_read, useGivenFilename=True):
        # prepend . if not present
        ext = extension
        if ext[0] != '.':
            ext = '.' + ext
        # get the given metadata file name
        if useGivenFilename and attr_metadataFile_read:
            fname = os.path.join(folder, self.Sanitize(attr_metadataFile_read))
            # append extension if not present
            if not fname.endswith(ext):
                fname += ext
        # otherwise use dataset name
        else:
            fname = os.path.join(folder, self.DefaultFileName(attr_proposal_read, attr_sampleName_read, attr_datasetName_read) + ext)
        # check if file exists, append number if it does
        x = 0
        xname = fname
        while os.path.isfile(xname):
            self.logger.warn("File already exists, will use an incremented file name. file=%s metadatamanager=%s" % (xname, self.metadataManagerName))
            xname = fname[:-(len(ext))] + '-' + str(x) + ext
            x += 1
        return xname

    # Returns the file on tehe output folder
    def getFiles(self, attr_dataFolder_read):
        try:
            return self.fileExplorer.getFiles(attr_dataFolder_read)
        except:
            traceback.print_exc(file=sys.stdout)
        return []



    def ExportToXMLFile(self, dataset, filepath):
        f = None
        # write the whole dataset in a xml file
        try:
            f = open(filepath, 'w-')
            # write the file
            dataset.export(f, 0, pretty_print=True)
            self.logger.info("Dataset exported as XML. filepath=%s metadatamanager=%s" % (filepath, self.metadataManagerName))
        except Exception as e:
            self.logger.error("Error writing metadata. file=%s error=%s metadatamanager=%s" % (filepath, e, self.metadataManagerName))
        finally:
            if f:
                try:
                    f.close()
                except Exception as r:
                    self.warn_stream("Error closing file. file=%s error=%s metadatamanager=%s" % (filepath,r, self.metadataManagerName))




