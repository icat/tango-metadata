'''
Created on Nov 7, 2014

@author: cleva
'''
import re
import logging
import json
import sys, traceback
import numpy

class ParameterDefinition():
    '''
    classdocs
    '''
    LIST_PARAMETER_REGEX = re.compile('\A\s*\[\s*[a-zA-Z0-9_\-]+(?:\s*,\s*[a-zA-Z0-9_\-]+)*\s*\]\s*\Z')
    REMOTE_PARAMETER_REG = re.compile('\A\s*([a-zA-Z0-9\.:_\-/]+)(?:\[([a-zA-Z0-9_\-/]+)\])?\s*\Z')
    SIMPLE = 'simple'
    LIST   = 'list'
    REMOTE = 'remote'
    MULTI  = 'multi'
    
    logger = logging.getLogger("ParameterDefinition")

    def __init__(self, parameterString):
        '''
        Constructor
        '''
        self.optionList = None
        self.remoteList = None
        self.originalString = parameterString
        # name = definition
        d = parameterString.split("=")
        self.parameterName = d[0].strip()
        # SIMPLE (no definition)
        if len(d) == 1:
            self.parameterDefinition = None
            self.parameterType = self.SIMPLE
        # LIST (list of options)
        elif self.LIST_PARAMETER_REGEX.match(d[1]):
            self.parameterDefinition = d[1].strip()
            self.parameterType = self.LIST
            self.optionList = []
            for s in self.parameterDefinition[1:-1].split(","):
                self.optionList.append(s.strip())
        # REMOTE (remote attribute(s))
        else:
            self.parameterDefinition = d[1].strip()
            self.remoteList = []
            # list of REMOTES
            r = self.parameterDefinition.split(",")
            if(len(r) > 1):
                self.parameterType = self.MULTI
            else:
                self.parameterType = self.REMOTE
            # build list of pairs (attribute, index)
            # index is None if absent
            for s in r:
                m = self.REMOTE_PARAMETER_REG.match(s)
                try:
                    g = m.groups()
                    # if index is digits, convert to int
                # list index vs. dict key
                # prevents using all digit strings as dict key
                    if g[1] and g[1].isdigit():
                        t = g[0], int(g[1])
                    else:
                        t = g[0], g[1]
                    self.remoteList.append(t)
                except:
                    print("No group found for parameter " + self.parameterName)
        
    def getOriginalString(self):
        return self.originalString
    
    def getParameterName(self):
        return self.parameterName

    def getParameterDefinition(self):
        return self.parameterDefinition
    
    def getParameterType(self):
        return self.parameterType
    
    def getOptionList(self):
        return self.optionList
    
    def getRemoteList(self):
        return self.remoteList
    
    def getRemoteAttributes(self):
        if self.remoteList is None:
            return None
        return [p[0] for p in self.remoteList]
    
    def isLocal(self):
        return self.parameterType == self.SIMPLE or self.parameterType == self.LIST
    
    def isRemote(self):
        return self.parameterType == self.REMOTE or self.parameterType == self.MULTI
    
    def getValue(self, attributeProxyMap):
        strrep = []
        for p in self.remoteList:
            s = 'ERROR'
            try:
                self.logger.debug('Remote attribute %s' % p[0])
                attribute = attributeProxyMap[p[0]]
                if attribute:

                    value = None

                    #Tuple type values can be now read from remote attribute and will be parsed to string
                    if type(attribute.read().value) is tuple:
                        value = ' '.join(attribute.read().value)              
                    else:                        
                        value = attribute.read().value

                    if p[1] is None:
                        s = attribute.get_config().format % value
                    elif type(p[1]) is int:
                        s = str(value[p[1]])
                    else:
                        s = str(json.loads(value)[p[1]])
                        
            except Exception as e:             
                traceback.print_exc(file=sys.stdout)               
            strrep.append(s)
        return ' '.join(strrep)

    def buildValue(self, attributeProxyMap, valueMap):
        strrep = []
        for p in self.remoteList:
            s = 'ERROR'
            try:
                attribute = attributeProxyMap[p[0]]
                if attribute:
                    if p[1] is None:
                        s = attribute.get_config().format % valueMap[p[0]]
                    elif type(p[1]) is int:
                        s = str(valueMap[p[0]][p[1]])
                    else:
                        s = str(json.loads(valueMap[p[0]])[p[1]])
            except Exception as e:
                self.logger.error("Error building attribute %s: %s" %(p[0], e))
            strrep.append(s)
        return ' '.join(strrep)

    
