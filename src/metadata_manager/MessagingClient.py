'''
Created on Oct 28, 2014

@author: cleva
'''
import sys
from stompest.config import StompConfig
from stompest.protocol import StompSession 
from stompest.protocol import StompSpec
from stompest.sync import Stomp
from datetime import datetime
from io import StringIO
import logging
import threading
import time
import urllib.request, urllib.error, urllib.parse
import base64
import json
import traceback




if sys.version_info < (3, 0):
   def toBytes(x):
       return x

else:
   def toBytes(x):
       if hasattr(x, "decode"):
           return x
       else:
           return x.encode("utf-8")

class StompClient():
    '''
    classdocs
    '''
    # Stomp client session state
    CONNECTING = StompSession.CONNECTING
    CONNECTED = StompSession.CONNECTED
    DISCONNECTING = StompSession.DISCONNECTING
    DISCONNECTED = StompSession.DISCONNECTED
    
    _TIMEOUT = 0.1
    
    JOLOKIA_CONSUMER_COUNT_URL = 'api/jolokia/read/org.apache.activemq:type=Broker,brokerName=metadata,destinationType=Queue,destinationName=icatIngest/ConsumerCount'
    JOLOKIA_PORT = 8778
    _JOLOKIA_USERNAME = 'user'
    _JOLOKIA_PASSWORD = 'user'
    
    def __init__(self, queueURLs, queueName, beamlineID, manager=None, jolokia_port=None):
        self.queueName = queueName
        self.beamlineID = beamlineID

        self.heartbeat = 0
        self.heartbeat_set = 0
        self.connected = False
        self.manager = manager        
        self.listener = None
        proxy_handler = urllib.request.ProxyHandler({})
        opener = urllib.request.build_opener(proxy_handler)
        urllib.request.install_opener(opener)

        if manager is None:
            self.logger = logging.getLogger("StompClient")
        else:
            self.logger = logging.getLogger("StompClient({})".format(manager.get_name()))

        # In case queueURLs is empty then messagingClient is set in status simulation
        self.isSimulationMode = False
        self._jolokia_port = jolokia_port
        if len(queueURLs) == 1:
            if queueURLs[0] == '':      
                self.isSimulationMode = True      
                self.logger.info("Messaging client to ActiveMQ is in SIMULATION mode. beamline=%s" %(beamlineID))
                self.cfgURL = "simulator"
                return 

        self.cfgURL = 'failover:(tcp://' + ',tcp://'.join(queueURLs) + ')'
        self.cfgURL += '?maxReconnectAttempts=3,initialReconnectDelay=250,maxReconnectDelay=1000'
        self.client = Stomp(StompConfig(self.cfgURL,version=StompSpec.VERSION_1_1))
        self.logger.info("Messaging client to ActiveMQ is connected. queueURLs=%s beamline=%s" % (queueURLs, beamlineID))

    @property
    def jolokia_port(self):
        if self._jolokia_port:
            return str(self._jolokia_port)
        else:
            return str(self.JOLOKIA_PORT)

    def getConfigURL(self):
        return self.cfgURL
    
    def getState(self):
        if self.isSimulationMode:
            return "connected"
        return self.client.session.state
    
    def _getManagerId(self):
        return self.manager.getID()
    
    def getStatus(self):
        if self.isSimulationMode:
            return "Connected to simulator"

        if self.connected:
            host = self.client._transport.host
            try:
                request = urllib.request.Request("http://%s:%s/%s" %(host, self.jolokia_port, self.JOLOKIA_CONSUMER_COUNT_URL))
                request.add_header('Authorization', b'Basic ' + base64.b64encode(toBytes(self._JOLOKIA_USERNAME + ':' + self._JOLOKIA_PASSWORD)))
                result = urllib.request.urlopen(request, timeout=1)
                jres = json.load(result)
                result.close()
                v = int(jres['value'])
                if v < 1:
                    s = 'No processor running'
                else:
                    s = '%s processors running' % v
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
                self.logger.error('Error getting Camel status: %s %s' % e, host)
                s = 'Processor status unknown'
            
            status = "Connected to %s at %s. server=%s host=%s queueName=%s" % (self.client.session.server, self.client.session.server, host, host, self.queueName)
            if s is not None:
                status = status + "\n" + s 
            return status
        else:
            return "Not connected to any server"
        
    def sendObject(self, xmlObj):
        xmlS = StringIO()
        xmlObj.export(xmlS, 0, pretty_print=False)             
        self.sendMessage(xmlS.getvalue())
        xmlS.close()
        
    def sendMessage(self, msg):
        if self.isSimulationMode:
            return

        # sends the msg in the queue
        # tries to reconnect after a 1st failed attempt
        # for failover to work
        try:           
           
            self.client.send(self.queueName, toBytes(msg), {'persistent':'true', StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL})
        except Exception as e:
            traceback.print_exc()
            self.logger.error("Error sending message, trying again: %s beamline=%s " % (e,self.beamlineID))
            self.beat()
            try:
                self.client.send(self.queueName, toBytes(msg), {'persistent':'true', StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL})
            except Exception as e:
                self.logger.error("Unable to send message. error=%s beamline=%s " % (e,self.beamlineID))

    def beat(self):
        if self.isSimulationMode:
            return
        # does a heartbeat if connected
        # maintaining a connection requires to call beat() at
        # a frequency below the hearbeat
        if self.client.session.state == StompSession.CONNECTED:
            # if the heartbeat changed, we disconnect
            if self.heartbeat == self.heartbeat_set:
                try:                    
                    self.client.beat()
                except Exception as e:
                    # if the heartbeat failed we close the connection
                    # this put the client in the correct DISCONNECTED state
                    try:
                        self.LOG.warn("Missed a heartBeat. beamline=%s" % str(self.beamlineID))
                        self.client.close(True)
                    except:
                        pass
            else:
                self.logger.info("Heart beat changed, will disconnect")
                self.client.disconnect()
        # after beat we call connect to ensure
        # the connection is reset if it was closed
        self.connect()
    
    def setHeartBeat(self, beat):
        self.heartbeat_set = beat
    
    def disconnect(self):
        if self.isSimulationMode:
            return
        if self.client.session.state == StompClient.CONNECTED:
            self.logger.info("Disconnecting. metadataManagerId=%s beamline=%s" % (self._getManagerId(), self.beamlineID))
            if self.listener:
                self.listener.stop()
            self.client.close(True)    
            self.connected = False 
    
    def connect(self):
        if self.isSimulationMode:
            return
        # if we disconnected or never connected, try to connect
        if self.client.session.state == StompSession.DISCONNECTED:
            try:
                self.heartbeat = self.heartbeat_set                
                self.client.connect(versions=[StompSpec.VERSION_1_1],heartBeats=(self.heartbeat,self.heartbeat),connectedTimeout=1)
                self.connected = True
                self.logger.info(self.getStatus().replace("\n", " - "))
                if self.heartbeat > 0:
                    self.logger.info("Heartbeat to: %d" % self.heartbeat)
            except Exception as e:
                self.logger.warn("Error connecting to ingestion queue. error=%s beamline=%s" % (e, self.beamlineID))
                traceback.print_exc()
                self.connected = False
            if self.connected and self.manager is not None:
                try:
                    topic = '/topic/beamline.' + self.manager.beamlineID.lower()
                    #self.logger.debug("Starting listener for %s. topic=%s beamline=%s" % (topic, topic, self.manager.beamlineID.lower()))
                    if self.listener:
                        self.listener.stop()
                    self.listener = _ListenerThread(self, topic, self.manager.beamlineID.lower())
                    self.listener.start()
                except Exception as e:
                    traceback.print_exc()
                    self.logger.warn("Error subscribing to %s: %s" % (topic, e))
                    self.listener=None
            
    def isConnected(self):
        if self.isSimulationMode:
            return True
        return self.connected
    
    def onReceive(self, frame):
        if self.manager is not None:
            self.manager.appendMessage("%s: %s" %(datetime.now().replace(microsecond=0), frame.body))

class _ListenerThread(threading.Thread):
    
    def __init__(self, parent, queue, beamline):
        threading.Thread.__init__(self)
        self.__stop = False
        self.setDaemon(True)
        self.parent = parent
        subsId = self.parent.manager.getID()
        self.parent.logger.debug("Subscribing. subsId=%s queue=%s beamline=%s" % (str(subsId), str(queue), str(beamline)))
        self.parent.client.subscribe(queue,{StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL, StompSpec.ID_HEADER: subsId})
        self.parent.logger.info("Subscribed. subsId=%s queue=%s beamline=%s" % (subsId, queue, beamline))

    def run(self):
        while not self.__stop:
            try:
                if self.parent.isConnected():
                    if self.parent.client.canRead(timeout=0.5):
                        frame = self.parent.client.receiveFrame()
                        self.parent.onReceive(frame)
                        self.parent.client.ack(frame)
                else:
                    time.sleep(0.5)
            except Exception as e:
                self.parent.logger.warn("Error in listener %s" %e)
                try:
                    self.parent.beat()
                except Exception:
                    pass
                time.sleep(0.5)
            
    def stop(self, wait=False):
        self.__stop = True
        if wait:
            self.join()
