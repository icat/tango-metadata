#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name='MetadataManager',
    version='4.0.4',
    description="ESRF Data Policy metadata manager and meta experiment Tango servers",
    # license="",
    # author="",
    # url="https://gitlab.esrf.fr/icat/tango-metadata",
    packages=["metadata_manager", "metadata_manager.icat_ingest_metadata"],
    package_dir={"":"src", "metadata_manager.icat_ingest_metadata": "src/metadata_manager/icat_ingest_metadata" },
    package_data={"metadata_manager": ["*.xml"]},
    entry_points={"console_scripts": [
        "MetadataManager = metadata_manager.MetadataManager:main",
        "MetaExperiment = metadata_manager.MetaExperiment:main",
    ]},
    install_requires=[
        "stompest",
        "h5py",
    ],
)
